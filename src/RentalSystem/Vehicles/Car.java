//============================================================
// Student Number	: S10149141B
// Student Name	: Jaryl Chng
// Module  Group	: IT01
//============================================================

// Car class
package RentalSystem.Vehicles;

import RentalSystem.Vehicle;

public class Car extends Vehicle  {
    // attributes
    private int childSeat; // number of child seats

    // constructors
    public Car() {}
    public Car(String regNo, String make, String model, double dailyRate, boolean available) {
        super(regNo, make, model, dailyRate, available);
    }

    // setters/getters
    // -> childSeat
    public void setChildSeat(int childSeat) {this.childSeat = childSeat;}
    public int getChildSeat() {return childSeat;}

    // abstract
    // -> calculate charges
    public double calculateCharges() {
        return dailyRate + (childSeat * 10.0); // each child seat costs $10 a day
    }

    // others
    // -> all attributes to string
    public String toString() {
        return "Car[" + super.toString() + ", Child seat(s): " + childSeat + "]";
    }
}