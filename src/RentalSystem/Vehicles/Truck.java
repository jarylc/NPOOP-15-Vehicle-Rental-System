//============================================================
// Student Number	: S10149141B
// Student Name	: Jaryl Chng
// Module  Group	: IT01
//============================================================

// Truck class
package RentalSystem.Vehicles;

import RentalSystem.Vehicle;

public class Truck extends Vehicle {
    // attributes
    private boolean driver; // driver required

    // constructors
    public Truck() {}
    public Truck(String regNo, String make, String model, double dailyRate, boolean available) {
        super(regNo, make, model, dailyRate, available);
    }

    // setters/getters
    // -> childSeat
    public void setDriver(boolean driver) {this.driver = driver;}
    public boolean getDriver() {return driver;}

    // abstract
    // -> calculate charges
    public double calculateCharges() {
        return dailyRate + (driver ? 100.0 : 0.0); // a driver costs $100 a day
    }

    // others
    // -> all attributes to string
    public String toString() {
        return "Truck[" + super.toString() + ", Driver: " + driver + "]";
    }
}