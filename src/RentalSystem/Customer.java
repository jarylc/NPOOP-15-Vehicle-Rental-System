//============================================================
// Student Number	: S10149141B
// Student Name	: Jaryl Chng
// Module  Group	: IT01
//============================================================

// Customer class
package RentalSystem;

// imports
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class Customer {
    // attributes
    private String icNo;
    private String name;
    private Calendar dob;
    private String tel;
    private ArrayList<Rental> rentalList;

    // constructors
    public Customer() {}
    public Customer(String icNo, String name, Calendar dob, String tel) {
        this.icNo = icNo;
        this.name = name;
        this.dob = dob;
        this.tel = tel;
        rentalList = new ArrayList<Rental>();
    }

    // set/get methods
    // -> icNo
    public void setICNo(String icNo) {this.icNo = icNo;}
    public String getICNo() {return icNo;}
    // -> name
    public void setName(String name) {this.name = name;}
    public String getName() {return name;}
    // -> dob
    public void setDOB(Calendar dob) {this.dob = dob;}
    public Calendar getDOB() {return dob;}
    // -> tel
    public void setTel(String tel) {this.tel = tel;}
    public String getTel() {return tel;}
    // -> rentalList (get only)
    public ArrayList<Rental> getRentalList() {return rentalList;}

    // other methods
    // -> add a rental to list of rentals of the customer
    public void addRental(Rental rental) {
        rentalList.add(rental);
    }
    // -> all attributes to string
    public String toString() {
        SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
        return "Customer[IC: " + icNo + ", Name: " + name + ", DOB: " + date.format(dob.getTime()) + ", Tel: " + tel + "]";
    }
}