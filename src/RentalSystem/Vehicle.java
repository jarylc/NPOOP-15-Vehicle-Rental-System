//============================================================
// Student Number	: S10149141B
// Student Name	: Jaryl Chng
// Module  Group	: IT01
//============================================================

// Vehicle class
package RentalSystem;

import java.text.DecimalFormat;

public abstract class Vehicle  {
    // attributes
    protected String regNo; // registration number
    protected String make; // make of vehicle
    protected String model; // model of vehicle
    protected double dailyRate; // daily rate
    protected boolean available; // available

    // constructors
    public Vehicle() {}
    public Vehicle(String regNo, String make, String model, double dailyRate, boolean available) {
        this.regNo = regNo;
        this.make = make;
        this.model = model;
        this.dailyRate = dailyRate;
        this.available = available;
    }

    // setters/getters
    // -> regNo
    public void setRegNo(String regNo) {this.regNo = regNo;}
    public String getRegNo() {return regNo;}
    // -> make
    public void setMake(String make) {this.make = make;}
    public String getMake() {return make;}
    // -> model
    public void setModel(String model) {this.model = model;}
    public String getModel() {return model;}
    // -> dailyRate
    public void setDailyRate(double dailyRate) {this.dailyRate = dailyRate;}
    public double getDailyRate() {return dailyRate;}
    // -> model
    public void setAvailable(boolean available) {this.available = available;}
    public boolean getAvailable() {return available;}

    // abstract
    // -> calculate charges
    public abstract double calculateCharges();

    // others
    // -> all attributes to string
    public String toString() {
        DecimalFormat money = new DecimalFormat("$0.00"); // money format
        return "Vehicle[Registration no.: " + regNo + ", Make: " + make + ", Model: " + model + ", Daily Rate: " + money.format(dailyRate) + ", Available: " + available + "]";
    }
}