//============================================================
// Student Number	: S10149141B
// Student Name	: Jaryl Chng
// Module  Group	: IT01
//============================================================

// Rental class
package RentalSystem;

import java.util.Calendar;

public class Rental {
    // attributes
    private static int counter; // counter (static to keep track for rental number)
    private int rentalNo; // rental number
    private Customer customer; // customer
    private Calendar pickupDate; // pickup date
    private Calendar returnDate; // return date
    private Vehicle vehicle; // vehicle

    // constructors
    public Rental() {}
    public Rental(Customer customer, Calendar pickupDate, Calendar returnDate, Vehicle vehicle) {
        counter++; // add 1 to counter
        rentalNo = counter; // assign new counter value to rentalNo.
        this.customer = customer;
        this.pickupDate = pickupDate;
        this.returnDate = returnDate;
        this.vehicle = vehicle;
    }

    // set/get methods
    // -> rentalNo
    public void setRentalNo(int rentalNo) {this.rentalNo = rentalNo;}
    public int getRentalNo() {return rentalNo;}
    // -> customer
    public void setCustomer(Customer customer) {this.customer = customer;}
    public Customer getCustomer() {return customer;}
    // -> pickupDate
    public void setPickupDate(Calendar date) {pickupDate = date;}
    public Calendar getPickupDate() {return pickupDate;}
    // -> returnDate
    public void setReturnDate(Calendar date) {returnDate = date;}
    public Calendar getReturnDate() {return returnDate;}
    // -> vehicle
    public void setVehicle(Vehicle vehicle) {this.vehicle = vehicle;}
    public Vehicle getVehicle() {return vehicle;}

    // other methods
    // -> calculate rental
    public double calculateRental() {
        long days = ((returnDate.getTimeInMillis() - pickupDate.getTimeInMillis()) / (1000 * 60 * 60 * 24)) + 1; // divide by milliseconds a day & +1 because return date is also counted (decimals automatically truncated)
        return days * vehicle.calculateCharges();
    }
    // -> all attributes to string
    public String toString() {
        return "Rental[Rental no.: " + rentalNo + ", Customer:" + customer.toString() + ", Pickup date: " + pickupDate.toString() + ", Return date: " + returnDate.toString() + "Vehicle: " + vehicle.toString() + "]";
    }
}