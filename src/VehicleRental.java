//============================================================
// Student Number	: S10149141B
// Student Name	: Jaryl Chng
// Module  Group	: IT01
// Notes : For this assignment, I assumed that:
//      - A customer must be a Singaporean, and have a Singapore contact number to be easily contactable.
//      - NRIC checksum when registering should be validated
//      - I have also replaced vehicle availability check with checking based on return date instead using a boolean.
//============================================================

// VehicleRental class (main)
import RentalSystem.*;
import RentalSystem.Vehicles.*;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import java.text.SimpleDateFormat;
import java.text.DecimalFormat;

class VehicleRental {
    // scanner for input (static and finalized as it is used throughout repeated and no change is needed)
    private static final Scanner in = new Scanner(System.in);

    // formatter variables (static and finalized to ensure consistency in formatting throughout)
    private static final SimpleDateFormat fDate = new SimpleDateFormat("dd/MM/yyyy");
    private static final DecimalFormat fMoney = new DecimalFormat("$0.00");

    // today's date
    private static Calendar today;

    // initial main method
    public static void main(String[] args) {
        // variables
        ArrayList<Customer> customerList = new ArrayList<Customer>(); // list of customers
        ArrayList<Vehicle> vehicleList = new ArrayList<Vehicle>(); // list of vehicles
        ArrayList<Rental> rentalList = new ArrayList<Rental>(); // list of rentals
        int opt; // user option

        // formatter settings
        fDate.setLenient(false); // set date formatter to check for leap years, invalid dates, etc

        initialize(customerList, vehicleList, rentalList);

        // initial greeting
        System.out.println("Welcome to Reliance Rental's Vehicle Renting System.");
        pause();

        // main loop - so the program won't end after finishing an option
        do {
            opt = displayMainMenu();

            resetToday(); // refresh today's date on every option
            refreshAvailabilities(vehicleList, rentalList); // refresh availabilities on every option

            System.out.println();
            switch(opt) {
                case 1:
                    registerCustomer(customerList);
                    break;
                case 2:
                    listCustomers(customerList);
                    break;
                case 3:
                    listVehicles(vehicleList, rentalList, false); // all vehicles
                    break;
                case 4:
                    listVehicles(vehicleList, rentalList, true); // only available vehicles
                    break;
                case 5:
                    rentVehicle(customerList, vehicleList, rentalList);
                    break;
                case 6:
                    listRentals(rentalList);
                    break;
                case 7:
                    returnVehicle(customerList, vehicleList);
                    break;
                case 8:
                    listHighestRental(vehicleList, rentalList);
                    break;
                case 9:
                    listCustomerRentals(customerList);
                    break;
                case 0:
                    System.out.println("Exiting...");
                    return; // end the program
                default:
                    printError("Option does not exist (Expected: 0 to 9)");
            }
            pause(); // pause for user to read every action
        } while (true);
    }
    // populate both customerList and vehicleList with some values for testing and assignment defaults
    private static void initialize(ArrayList<Customer> customerList, ArrayList<Vehicle> vehicleList, ArrayList<Rental> rentalList) {
        // customerList
        customerList.add(new Customer("S9099999A", "Adrian", new GregorianCalendar(1990, 2, 22), "91112222"));
        customerList.add(new Customer("S8558911B", "Benjamin", new GregorianCalendar(1989, 7, 11), "96668888"));
        customerList.add(new Customer("S7670326C", "Christina", new GregorianCalendar(1979, 11, 13), "93335555"));

        // vehicleList
        vehicleList.add(new Car("SJV1883R", "Honda", "Civic", 60.00, true));
        vehicleList.add(new Car("SJZ2987A", "Toyota", "Altis", 60.00, true));
        vehicleList.add(new Car("SKA4370H", "Honda", "Accord", 80.00, true));
        vehicleList.add(new Car("SKD8024M", "Toyota", "Camry", 80.00, true));
        vehicleList.add(new Car("SKH5922D", "BMW", "320i", 90.00, true));
        vehicleList.add(new Car("SKM5139C", "BMW", "520i", 100.00, true));
        vehicleList.add(new Car("SKP8899H", "Mercedes", "S500", 300.00, true));
        vehicleList.add(new Truck("GB3221K", "Tata", "Magic", 200.00, true));
        vehicleList.add(new Truck("YB8283M", "Isuzu", "NPR", 250.00, true));
        vehicleList.add(new Truck("YK5133H", "Isuzu", "NQR", 300.00, true));

        // rentalList
        /*rentalList.add(new Rental(customerList.get(1), new GregorianCalendar(2014, 10, 3), new GregorianCalendar(2014, 10, 4),  vehicleList.get(7))); // Benjamin - GB3221K
        customerList.get(1).addRental(rentalList.get(0));
        rentalList.add(new Rental(customerList.get(0), new GregorianCalendar(2014, 11, 5), new GregorianCalendar(2014, 11, 6),  vehicleList.get(8))); // Adrian - YB8283M
        customerList.get(0).addRental(rentalList.get(1));
        rentalList.add(new Rental(customerList.get(0), new GregorianCalendar(2014, 11, 24), new GregorianCalendar(2014, 11, 26),  vehicleList.get(1))); // Adrian - SJZ2987A
        customerList.get(0).addRental(rentalList.get(2));
        rentalList.add(new Rental(customerList.get(1), new GregorianCalendar(2015, 0, 9), new GregorianCalendar(2015, 0, 10), vehicleList.get(8))); // Benjamin - YB8283M
        customerList.get(1).addRental(rentalList.get(3));
        rentalList.add(new Rental(customerList.get(1), new GregorianCalendar(2014, 11, 30), new GregorianCalendar(2015, 0, 2), vehicleList.get(1))); // Benjamin - SHZ2987A
        customerList.get(1).addRental(rentalList.get(4));
        rentalList.add(new Rental(customerList.get(2), new GregorianCalendar(2015, 0, 17), new GregorianCalendar(2015, 0, 18), vehicleList.get(5))); // Christina - SKM5139C
        customerList.get(2).addRental(rentalList.get(5));*/

        // according to Mr Ng Weng Choh's rentalList initialization
        rentalList.add(new Rental(customerList.get(0), new GregorianCalendar(2015, 0, 1), new GregorianCalendar(2015, 0, 3), vehicleList.get(0)));
        ((Car) vehicleList.get(0)).setChildSeat(1);
        customerList.get(0).addRental(rentalList.get(0));
        rentalList.add(new Rental(customerList.get(1), new GregorianCalendar(2015, 1, 1), new GregorianCalendar(2015, 1, 2), vehicleList.get(1)));
        ((Car) vehicleList.get(1)).setChildSeat(0);
        customerList.get(1).addRental(rentalList.get(1));
        rentalList.add(new Rental(customerList.get(2), new GregorianCalendar(2015, 1, 4), new GregorianCalendar(2015, 1, 6), vehicleList.get(4)));
        ((Car) vehicleList.get(4)).setChildSeat(2);
        customerList.get(2).addRental(rentalList.get(2));
        rentalList.add(new Rental(customerList.get(0), new GregorianCalendar(2014, 11, 4), new GregorianCalendar(2014, 11, 6), vehicleList.get(8)));
        ((Truck) vehicleList.get(8)).setDriver(true);
        customerList.get(0).addRental(rentalList.get(3));
        rentalList.add(new Rental(customerList.get(2), new GregorianCalendar(2014, 10, 4), new GregorianCalendar(2014, 10, 6), vehicleList.get(8)));
        ((Truck) vehicleList.get(8)).setDriver(false);
        customerList.get(2).addRental(rentalList.get(4));
    }
    // display main menu
    private static int displayMainMenu() {
        do {
            System.out.print("MENU\n=====\n1. Register customer\n2. List all customers\n3. List all vehicles\n4. List all available vehicles\n5. Rent a vehicle\n6. List all rental details for all cars or trucks\n7. Return vehicle\n8. List most rented cars or trucks\n9. List rentals of customer over a period\n0. Exit\nEnter your option: ");
            try {
                return Integer.parseInt(in.nextLine());
            } catch (Exception ex) {
                printError("Option input is not valid (Expected: Option number)");
                pause();
            }
        } while (true);
    }

    // option 1 - register customer (BASIC REQUIREMENT 1)
    private static void registerCustomer(ArrayList<Customer> customerList) {
        // input variables
        String nric, name, tel;
        Calendar dob;

        // input
        // -> nric
        do {
            System.out.print("Enter NRIC number: ");
            nric = in.nextLine().toUpperCase();

            // validation
            // -> not empty & matches pattern of (LETTER)#######(LETTER)
            if (nric.trim().length() > 0 && nric.matches("[A-Z][0-9]{7}[A-Z]")) {
                // -> not duplicated
                Customer search = findByNRIC(customerList, nric);
                if (search == null) {
                    // -> valid checksum
                    char checksum = generateNRICChecksum(nric);
                    if (nric.charAt(8) == checksum) {
                        break; // continue to next step
                    } else {
                        printError("Checksum is invalid, please re-check (Expected: " + nric.replace(nric.charAt(8), checksum) + ")");
                    }
                } else {
                    printError("\"" + nric + "\" (" + search.getName() + ") already exists in the database");
                }
            } else {
                printError("Input is not valid (Expected: NRIC number)");
            }
            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);
        // -> name
        do {
            System.out.print("Enter name: ");
            name = in.nextLine();

            // validation
            // -> not empty
            if (name.trim().length() > 0) {
                break; // continue to next step
            } else {
                printError("Input is not valid (Expected: Name)");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);
        // -> dob
        do {
            System.out.print("Enter date of birth (DD/MM/YYYY): ");
            dob = Calendar.getInstance();
            // validation
            // -> try-catch (Date)
            try {
                dob.setTime(fDate.parse(in.nextLine()));

                // -> 18 years old and above (removed, felt like it doesn't make sense to check this after awhile since a customer can rent after his 18th birthday)
                /*Calendar today = new GregorianCalendar();
                int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
                if (today.get(Calendar.MONTH) < dob.get(Calendar.MONTH) || (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && today.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH))) {
                    age--; // if the birthday hasn't passed, the user is still (years difference - 1) years old
                }
                if (age >= 18) {
                    break;
                } else {
                    printError("Customer (" + age + ") must be 18 or above to register and be eligible to drive in Singapore");
                }*/

                // -> before today's date
                if (dob.compareTo(today) == -1) {
                    break;
                } else {
                    printError("Date must be before today's date (Expected: Before " + fDate.format(today.getTime()) + ")");
                }
            } catch (Exception ex) {
                printError("Input is not valid (Expected: Date (Format: DD/MM/YYYY))");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);
        // -> tel
        do {
            System.out.print("Enter contact number (Singapore): +65 ");
            tel = in.nextLine();

            // validation
            // -> not empty && 8 digits and starts with 6, 7 (from 2015), 8, or 9
            if (tel.trim().length() > 0 && tel.matches("[6-9][0-9]{7}")) {
                break; // continue to next step
            } else {
                printError("Input is not valid (Expected: Singapore contact number)");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);

        // add new customer to database
        Customer c = new Customer(nric, name, dob, tel);
        customerList.add(c);

        // success message
        System.out.println(name + " (" + nric + ") has been successfully added to the customer database");
    }

    // option 2 - list customers (BASIC REQUIREMENT 2)
    private static void listCustomers(ArrayList<Customer> customerList) {
        int no = 0; // counter

        System.out.println("=== List of customers ===");
        System.out.println("S/No  IC number  Name                  Date of birth  Contact");
        for (Customer c : customerList) {
            no++;
            System.out.printf("%4d  %-9s  %-20s  %-13s  %-8s\n", no, c.getICNo(), c.getName(), fDate.format(c.getDOB().getTime()), c.getTel());
        }
        System.out.println(customerList.size() + " customer(s) retrieved from database");
    }

    // option 3 & 4 - list all vehicles or only available vehicles (BASIC REQUIREMENT 3 & 4)
    private static void listVehicles(ArrayList<Vehicle> vehicleList, ArrayList<Rental> rentalList, boolean onlyAvailable) {
        int no = 0; // counter

        System.out.println("=== List of" + (onlyAvailable ? " available " : " ") + "vehicles ===");
        System.out.println("S/No  Type   Reg number  Make        Model       Daily rate" + (!onlyAvailable ? "  Available" : ""));
        for (Vehicle v : vehicleList) {
            Rental curRental = getCurrentRental(rentalList, v); // current rental number of vehicle, 0 if none

            if (!onlyAvailable || v.getAvailable()) {
                no++;

                System.out.printf("%4d  %-5s  %-10s  %-10s  %-10s  %-10s", no, (v instanceof Car ? "Car" : "Truck"), v.getRegNo(), v.getMake(), v.getModel(), fMoney.format(v.getDailyRate()));
                if (!onlyAvailable) {
                    System.out.printf("  %-9s", (curRental == null ? "Yes" : "No (Rental " + curRental.getRentalNo() + ")"));
                }
                System.out.println();
            }
        }
        System.out.println(no + " vehicle(s) retrieved from database");
    }

    // option 5 - rent a vehicle (BASIC REQUIREMENT 5)
    private static void rentVehicle(ArrayList<Customer> customerList, ArrayList<Vehicle> vehicleList, ArrayList<Rental> rentalList) {
        int custNo, vehicleNo, childSeats; // customer s/no, vehicle s/no, no of child seats required
        String driver; // driver required
        Customer cust; // customer selected
        Vehicle vehicle; // vehicle selected
        Calendar pickupDate, returnDate; // pickup date, return date

        ArrayList<Rental> advanceRentals; // advanced rentals of selected vehicle (if any)

        // input
        // -> custNo
        do {
            listCustomers(customerList);
            System.out.print("Enter S/No of customer: ");
            // validation
            // -> try-catch (integer check)
            try {
                custNo = Integer.parseInt(in.nextLine());

                // -> input is in range
                if (custNo >= 1 && custNo <= customerList.size()) {
                    cust = customerList.get(custNo - 1);
                    break;
                } else {
                    printError("Invalid customer number (Expected: 1 to " + customerList.size() + ")");
                }
            } catch (Exception ex) {
                printError("Input is not valid (Expected: Customer S/No)");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);
        // -> vehicleNo
        do {
            System.out.println(); // line spacing for aesthetics
            listVehicles(vehicleList, rentalList, false); // list all vehicles
            System.out.print("Enter S/No of vehicle: ");
            // validation
            // -> try-catch (integer check)
            try {
                vehicleNo = Integer.parseInt(in.nextLine());

                // -> input is in range
                if (vehicleNo >= 1 && vehicleNo <= vehicleList.size()) {
                    // -> vehicle is available
                    vehicle = vehicleList.get(vehicleNo - 1);
                    Rental curRental = getCurrentRental(rentalList, vehicle); // current rental number of vehicle, 0 if none
                    if (curRental == null) {
                        break;
                    } else {
                        printError("Vehicle is currently on rent (Rental " + curRental.getRentalNo() + ")");
                    }
                } else {
                    printError("Invalid vehicle number (Expected: 1 to " + vehicleList.size() + ")");
                }
            } catch (Exception ex) {
                printError("Input is not valid (Expected: Vehicle S/No)");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);

        // attempt to find if selected vehicle has an advance rentals
        advanceRentals = getAdvanceRentals(rentalList, vehicle);
        if (advanceRentals.size() > 0) {
            System.out.println("NOTE: Vehicle has advance rental(s):");
            for (Rental r : advanceRentals) {
                System.out.println("\tRental " + r.getRentalNo() + ": " + fDate.format(r.getPickupDate().getTime()) + " to " + fDate.format(r.getReturnDate().getTime()));
            }
        }

        // -> pickupDate
        do {
            System.out.print("Enter pickup date (DD/MM/YYYY): ");
            pickupDate = Calendar.getInstance();
            // validation
            // -> try-catch (Date)
            try {
                pickupDate.setTime(fDate.parse(in.nextLine()));
                // -> if pickup date is today or after
                if (today.compareTo(pickupDate) <= 0) {
                    // -> if pickup date does not clash with any rentals
                    if (getRentalsFromDateRange(rentalList, vehicle, pickupDate, pickupDate).size() == 0) {
                        break;
                    } else {
                        printError("Pickup date is in between another rental of this vehicle");
                    }
                } else {
                    printError("Pickup date cannot be before today (Expected: " + fDate.format(today.getTime()) + " or after)");
                }

            } catch (Exception ex) {
                printError("Input is not valid (Expected: Date (Format: DD/MM/YYYY))");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);
        // -> returnDate
        do {
            System.out.print("Enter return date (DD/MM/YYYY): ");
            returnDate = Calendar.getInstance();
            // validation
            // -> try-catch (Date)
            try {
                returnDate.setTime(fDate.parse(in.nextLine()));
                // -> return date is after or on pickup date
                if (returnDate.compareTo(pickupDate) >= 0) {
                    // -> if return date does not clash with any rentals
                    if (getRentalsFromDateRange(rentalList, vehicle, pickupDate, returnDate).size() == 0) {
                        break;
                    } else {
                        printError("Rental date range overlaps with one or more rentals of this vehicle");
                    }
                } else {
                    printError("Date must the pickup date or after (Expected: " + fDate.format(pickupDate.getTime()) + " or after)");
                }
            } catch (Exception ex) {
                printError("Input is not valid (Expected: Date (Format: DD/MM/YYYY))");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);

        // if car, ask for number of child seats
        if (vehicle instanceof Car) {
            // -> childSeats
            do {
                System.out.print("Enter number of child seats: ");
                // validation
                // -> try-catch (integer check)
                try {
                    childSeats = Integer.parseInt(in.nextLine());

                    // -> input is in range
                    if (childSeats >= 0) {
                        ((Car) vehicle).setChildSeat(childSeats);
                        break;
                    } else {
                        printError("Invalid number of child seats (Expected: 0 or more)");
                    }
                } catch (Exception ex) {
                    printError("Input is not valid (Expected: Number of child seats)");
                }

                // validation fails
                if (!retry()) {
                    return; // go back to menu if user chooses not to retry
                }
            } while (true);
        } else if (vehicle instanceof Truck) { // if truck, ask if driver is needed
            // -> driver
            do {
                System.out.print("Driver required? (Y/N): ");
                driver = in.nextLine().toUpperCase();
                if (driver.equals("Y") || driver.equals("N")) {
                    ((Truck) vehicle).setDriver(driver.equals("Y"));
                    break;
                } else {
                    printError("Input is not valid (Expected: 'Y' or 'N')");
                }

                // validation fails
                if (!retry()) {
                    return; // go back to menu if user chooses not to retry
                }
            } while (true);
        }

        Rental rental = new Rental(cust, pickupDate, returnDate, vehicle);
        cust.addRental(rental);
        rentalList.add(rental);

        System.out.println("Total charges: " + fMoney.format(rental.calculateRental()));
    }

    // option 6 - list rental details for all cars or trucks (BASIC REQUIREMENT 6)
    private static void listRentals(ArrayList<Rental> rentalList) {
        int type, no = 0; // vehicle type (1-Car, 2-Truck), counter

        // input
        // -> type
        do {
            System.out.print("Enter vehicle type number (1-Car, 2-Truck): ");
            // validation
            // -> try-catch (integer check)
            try {
                type = Integer.parseInt(in.nextLine());

                // -> input is 1 or 2
                if (type == 1 || type == 2) {
                    break;
                } else {
                    printError("Invalid vehicle type number (Expected: 1 (Car) or 2 (Truck))");
                }
            } catch (Exception ex) {
                printError("Input is not valid (Expected: 1 (Car) or 2 (Truck))");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);

        System.out.println("==== List of rentals (" + (type == 1 ? "Car" : "Truck") + ") ====");
        System.out.println("Rental  Customer name         Reg number  Make        Model       Pick up     Return");
        for (Rental r : rentalList) {
            Vehicle v = r.getVehicle(); // vehicle of rental
            // show only cars if user selected 1 or show only trucks if user selected 2 (if 0 show all)
            if (type != 0 & ((type == 1 && (v instanceof Car)) || (type == 2 && (v instanceof Truck)))) {
                no++;

                System.out.printf("%6d  %-20s  %-10s  %-10s  %-10s  %-10s  %-10s\n", r.getRentalNo(), r.getCustomer().getName(), v.getRegNo(), v.getMake(), v.getModel(), fDate.format(r.getPickupDate().getTime()), fDate.format(r.getReturnDate().getTime()));
            }
        }
        System.out.println(no + " rental(s) retrieved from database");
    }

    // option 7 - return vehicle (ADVANCED REQUIREMENT 1)
    private static void returnVehicle(ArrayList<Customer> customerList, ArrayList<Vehicle> vehicleList) {
        String nric, vehicleRegNo; // nric to search, registration number of vehicle to search
        Customer customer; // customer found by nric
        Vehicle vehicle; // vehicle found by registration number
        Rental rental; // current rental of vehicle found
        Calendar returnDate;

        // input
        // -> nric
        do {
            System.out.print("Enter NRIC number: ");
            nric = in.nextLine().toUpperCase();

            // validation
            // -> not empty & matches pattern of (LETTER)#######(LETTER)
            if (nric.trim().length() > 0 && nric.matches("[A-Z][0-9]{7}[A-Z]")) {
                // -> customer with nric found
                customer = findByNRIC(customerList, nric);
                if (customer != null) {
                    break; // continue to next step
                } else {
                    printError("\"" + nric + "\" does not exists in the customer database");
                }
            } else {
                printError("Input is not valid (Expected: NRIC number)");
            }
            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);
        // -> vehicleReg
        do {
            System.out.print("Enter vehicle registration number: ");
            vehicleRegNo = in.nextLine().toUpperCase();

            // validation
            // -> input not blank
            if (vehicleRegNo.trim().length() > 0) {
                // -> vehicle found in customer's rentals
                vehicle = findVehicleByRegNo(vehicleList, vehicleRegNo);
                if(vehicle != null) {
                    // -> vehicle found is still on rent by customer
                    rental = getCurrentRental(customer.getRentalList(), vehicle);
                    if (rental != null) {
                        break;
                    } else {
                        printError("\"" + vehicleRegNo + "" + " is currently not being rented by " + customer.getName());
                    }
                } else {
                    printError("\"" + vehicleRegNo + "\" does not exists in the vehicle database");
                }
            } else {
                printError("Input is not valid (Expected: Vehicle registration number)");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);

        // list details of selected rental
        System.out.println();
        System.out.println("Selected rental: " + vehicle.getMake() + " " + vehicle.getModel());
        System.out.println("Rented: " + fDate.format(rental.getPickupDate().getTime()) + " to " + fDate.format(rental.getReturnDate().getTime()));

        // -> returnDate
        do {
            System.out.print("Enter new return date (DD/MM/YYYY): ");
            returnDate = Calendar.getInstance();
            // validation
            // -> try-catch (Date)
            try {
                returnDate.setTime(fDate.parse(in.nextLine()));
                // -> return date is after or on pickup date
                if (returnDate.compareTo(rental.getPickupDate()) >= 0) {
                    break;
                } else {
                    printError("Date must the pickup date or after (Expected: " + fDate.format(rental.getPickupDate().getTime()) + " or after)");
                }
            } catch (Exception ex) {
                printError("Input is not valid (Expected: Date (Format: DD/MM/YYYY))");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);

        rental.setReturnDate(returnDate);
        System.out.println("Updated charges: " + fMoney.format(rental.calculateRental()));
    }

    // option 8 - List car or truck with the highest number of rental transactions (ADVANCED REQUIREMENT 2)
    private static void listHighestRental(ArrayList<Vehicle> vehicleList, ArrayList<Rental> rentalList) {
        int type, curHighest = 0, no = 0; // vehicle type (1-Car, 2-Truck), current highest count found, counter
        int[] cRental = new int[vehicleList.size()]; // parallel array to count vehicle rentals

        // input
        // -> type
        do {
            System.out.print("Enter vehicle type number (1-Car, 2-Truck): ");
            // validation
            // -> try-catch (integer check)
            try {
                type = Integer.parseInt(in.nextLine());

                // -> input is 1 or 2
                if (type == 1 || type == 2) {
                    break;
                } else {
                    printError("Invalid vehicle type number (Expected: 1 (Car) or 2 (Truck))");
                }
            } catch (Exception ex) {
                printError("Input is not valid (Expected: 1 (Car) or 2 (Truck))");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);

        // initialize values of rental count with 0
        for (int i = 0; i < cRental.length; i++) {
            cRental[i] = 0;
        }
        // start counting rentals
        for (Rental r : rentalList) {
            Vehicle v = r.getVehicle();
            if ((type == 1 && (v instanceof Car)) || (type == 2 && (v instanceof Truck))) {
                int i = vehicleList.indexOf(v);
                cRental[i] += 1; // add 1 rental count to the vehicle, initialize with 1 if null
                curHighest = (cRental[i] > curHighest ? cRental[i] : curHighest); // if the count is higher than the current found highest, set it as the current found highest
            }
        }

        System.out.println("==== " + (type == 1 ? "Car" : "Truck") + "s with the highest rental count (Record: " + curHighest + ") ====");
        System.out.println("Reg number  Make        Model       Daily rate  Available");
        for (int i = 0; i < cRental.length; i++) {
            if (cRental[i] == curHighest) {
                Vehicle v = vehicleList.get(i);
                Rental curRental = getCurrentRental(rentalList, v); // current rental number of vehicle, 0 if none
                no++;

                System.out.printf("%-10s  %-10s  %-10s  %-10s  %-9s\n", v.getRegNo(), v.getMake(), v.getModel(), fMoney.format(v.getDailyRate()), (curRental == null ? "Yes" : "No (Rental " + curRental.getRentalNo() + ")"));
            }
        }
    }

    // option 9 - List rental details of a customer over a period (ADVANCED REQUIREMENT 3)
    public static void listCustomerRentals(ArrayList customerList) {
        String nric; // nric to search
        Customer customer; // customer found by nric
        Calendar startDate, endDate; // start date to search, till end date to search
        ArrayList<Rental> cRentals; // customer's rentals over the date specified

        // input
        // -> nric
        do {
            System.out.print("Enter NRIC number: ");
            nric = in.nextLine().toUpperCase();

            // validation
            // -> not empty & matches pattern of (LETTER)#######(LETTER)
            if (nric.trim().length() > 0 && nric.matches("[A-Z][0-9]{7}[A-Z]")) {
                // -> customer with nric found
                customer = findByNRIC(customerList, nric);
                if (customer != null) {
                    break; // continue to next step
                } else {
                    printError("\"" + nric + "\" does not exists in the customer database");
                }
            } else {
                printError("Input is not valid (Expected: NRIC number)");
            }
            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);
        // -> startDate
        do {
            System.out.print("Enter start date (DD/MM/YYYY): ");
            startDate = Calendar.getInstance();
            // validation
            // -> try-catch (Date)
            try {
                startDate.setTime(fDate.parse(in.nextLine()));
                break;
            } catch (Exception ex) {
                printError("Input is not valid (Expected: Date (Format: DD/MM/YYYY))");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);
        // -> returnDate
        do {
            System.out.print("Enter return date (DD/MM/YYYY): ");
            endDate = Calendar.getInstance();
            // validation
            // -> try-catch (Date)
            try {
                endDate.setTime(fDate.parse(in.nextLine()));
                // -> return date is after or on start date
                if (endDate.compareTo(startDate) >= 0) {
                    break;
                } else {
                    printError("Date must the start date or after (Expected: " + fDate.format(startDate.getTime()) + " or after)");
                }
            } catch (Exception ex) {
                printError("Input is not valid (Expected: Date (Format: DD/MM/YYYY))");
            }

            // validation fails
            if (!retry()) {
                return; // go back to menu if user chooses not to retry
            }
        } while (true);

        System.out.println("==== List of rentals to " + customer.getName() + " (" + nric + ") from " + fDate.format(startDate.getTime()) + " to " + fDate.format(endDate.getTime()) + " ====");
        System.out.println("Rental  Reg number  Type   Make        Model       Pick up     Return");

        cRentals = getRentalsFromDateRange(customer.getRentalList(), null, startDate, endDate);
        for (Rental r : cRentals) {
            Vehicle v = r.getVehicle();
            System.out.printf("%6d  %-10s  %-5s  %-10s  %-10s  %-10s  %-10s\n", r.getRentalNo(), v.getRegNo(), (v instanceof Car ? "Car" : "Truck"),v.getMake(), v.getModel(), fDate.format(r.getPickupDate().getTime()), fDate.format(r.getReturnDate().getTime()));
        }
        System.out.println(cRentals.size() + " rental(s) retrieved from database");
    }

    // UTILITIES (useful functions that are used throughout)
    // -> generate checksum character from NRIC
    private static char generateNRICChecksum(String nric) {
        int check = 0; // checksum value

        // convert digits to checksum value
        // -> extract all digits from NRIC, multiply by weights 2 7 6 5 4 3 2 accordingly and sum all values
        for (int i = 1; i < nric.length() - 1; i++) {
            check += (nric.charAt(i) - 48) * (i == 1 ? 2 : 9 - i);
        }
        // -> modulus by 11
        check %= 11;
        // -> have 11 - the current value
        check = 11 - check;

        // convert checksum value to character
        if (check < 10) {
            return (char) (64 + check); // 1 = A (65), 2 = B (66), ..., 9 = I (73)
        } else {
            return (check == 10 ? 'Z' : 'J'); // 10 = Z, 11 = J
        }
    }
    // -> check if NRIC exists in database and return the customerList index (returns -1 if not found)
    private static Customer findByNRIC(ArrayList<Customer> customerList, String nric) {
        for (Customer c : customerList) {
            if (c.getICNo().equals(nric)) {
                return c;
            }
        }
        return null;
    }

    // -> refresh availabilities of all vehicles, to not make it totally redundant as I'm doing by dates instead of boolean
    private static void refreshAvailabilities(ArrayList<Vehicle> vehicleList, ArrayList<Rental> rentalList) {
        // firstly, set all vehicles to available
        for (Vehicle v : vehicleList) {
            v.setAvailable(true);
        }

        // then, check the rental list to set unavailable vehicles
        for (Rental r : rentalList) {
            // if pickup date is today or before & return date of the vehicle is today or has not passed yet
            if (today.compareTo(r.getPickupDate()) >= 0 && today.compareTo(r.getReturnDate()) <= 0) {
                r.getVehicle().setAvailable(false);
            }
        }
    }
    // -> get the current rental object of a vehicle if it is currently still on rent using dates (returns null if not found)
    private static Rental getCurrentRental(ArrayList<Rental> rentalList, Vehicle vehicle) {
        for (Rental r : rentalList) {
            // if pickup date is today or before & return date of the vehicle is today or has not passed yet
            if (r.getVehicle() == vehicle && (today.compareTo(r.getPickupDate()) >= 0 && today.compareTo(r.getReturnDate()) <= 0)) {
                return r;
            }
        }
        return null;
    }
    // -> get the advance rentals of a specific vehicle (returns null if not found)
    private static ArrayList<Rental> getAdvanceRentals(ArrayList<Rental> rentalList, Vehicle vehicle) {
        ArrayList<Rental> rentals = new ArrayList<Rental>(); // rentals found
        for (Rental r : rentalList) {
            // if pickup date is in the future
            if (r.getVehicle() == vehicle && (r.getPickupDate().compareTo(today) > 0)) {
                rentals.add(r);
            }
        }
        return rentals;
    }

    // realised this was not required because I could still use the below method
    /*// -> get rental of a specific vehicle on a date (returns null not found)
    private static Rental getRentalFromDate(ArrayList<Rental> rentalList, Vehicle vehicle, Calendar cal) {
        for (Rental r : rentalList) {
            // if the date is in-between dates of any future rentals
            if (r.getVehicle() == vehicle && (cal.compareTo(r.getPickupDate()) >= 0 && cal.compareTo(r.getReturnDate()) <= 0)) {
                return r;
            }
        }

        return null;
    }*/
    // -> get rentals between 2 dates [overlaps] (enter null for vehicle to search all)
    private static ArrayList getRentalsFromDateRange(ArrayList<Rental> rentalList, Vehicle vehicle, Calendar startDate, Calendar endDate) {
        ArrayList<Rental> rentals = new ArrayList<Rental>();
        for (Rental r : rentalList) {
            // if specific vehicle is specified, search for specified vehicle only
            if (vehicle == null || r.getVehicle() == vehicle) {
                // if the (earlier date is in within a rental) or the (pickup date is equal or less than earlier date & return date is equal or more than the earlier date and still equal or less than the later date)

                // (start <= pickup <= end) || (start <= return <= end) || (start <= pickup && end >= return)
                Calendar pud = r.getPickupDate();
                Calendar rtd = r.getReturnDate();
                if ((startDate.compareTo(pud) >= 0 && startDate.compareTo(rtd) <= 0) || (endDate.compareTo(pud) >= 0 && endDate.compareTo(rtd) <= 0) || (startDate.compareTo(pud) <= 0 && endDate.compareTo(rtd) >= 0)) {
                    rentals.add(r);
                }
            }
        }
        return rentals;
    }

    /* initially for option 5, but list all vehicles is cleaner and in the requirements
    // -> count available vehicles
    private static int countAvailableVehicles(ArrayList<Vehicle> vehicleList, ArrayList<Rental> rentalList) {
        int count = 0; // number of available vehicles

        for (Rental v : vehicleList) {
            int rent = getCurrentRentalNo(rentalList, v); // current rental number of vehicle, 0 if none

            if (rent == 0) {
                count++;
            }
        }

        return count;
    }
    // -> get vehicle from the available vehicles list
    private static Vehicle getAvailableVehicle(ArrayList<Vehicle> vehicleList, ArrayList<Rental> rentalList, int no) {
        int count = 0; // number of available vehicles

        for (Vehicle v : vehicleList) {
            int rent = getCurrentRentalNo(rentalList, v); // current rental number of vehicle, 0 if none

            if (rent == 0) {
                count++;
                if (count == no) {
                    return v;
                }
            }
        }
        return null;
    }*/

    // -> find a vehicle in the database by registration number (returns null if not found)
    private static Vehicle findVehicleByRegNo(ArrayList<Vehicle> vehicleList, String vehicleRegNo) {
        for (Vehicle v : vehicleList) {
            // if registration number found
            if (v.getRegNo().equals(vehicleRegNo)) {
                return v;
            }
        }
        return null;
    }

    // -> reset today variable with today's date only without timestamp (today at midnight)
    private static void resetToday() {
        today = new GregorianCalendar();

        // reset timestamp values
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
    }

    // -> used to allow users to read messages before continuing
    private static void pause() {
        System.out.print("\nPress the ENTER key to continue...");
        in.nextLine();
        System.out.println();
    }
    // -> prompt the user if he would like to retry previous action (mostly used when user input fails validation)
    private static boolean retry() {
        String retry;
        do {
            System.out.print("Continue previous action? (Y/N): ");
            retry = in.nextLine().toUpperCase();
            if (retry.equals("Y") || retry.equals("N")) {
                if (retry.equals("N")) {
                    System.out.println("Aborting and going back to menu...");
                }
                break;
            }
            printError("Invalid input (Expected: 'Y' or 'N')");
        } while (true);

        return (retry.equals("Y")); // "Y" = true; "N" = false
    }
    // -> print line with the "ERROR: " prefix
    private static void printError (String error) {
        System.out.println("ERROR: " + error);
    }
}